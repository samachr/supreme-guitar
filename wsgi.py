import os
import json
from flask import Flask, render_template, request
application = Flask(__name__)

import soup

ipaddr = os.getenv("IP", "0.0.0.0")
port = int(os.getenv("PORT", 8080))

# @app.route('/song', methods = ['POST', 'GET'])
# def getsong():
#     song = request.args.get("search")
#     print "getting ", song
#     try:
#         return soup.grabtab(song)
#     except Exception as e:
#         return "error getting " + song + ". error is:" + e.message

@application.route('/api/find', methods = ['POST', 'GET'])
def getsongbaselink():
    song = request.args.get("search")
    print "searching ", song
    try:
        return soup.find_song(song)
    except Exception as e:
        return "error getting " + song + ". error is:" + e.message

@application.route('/api/song', methods = ['POST', 'GET'])
def getsonglink():
    song = request.args.get("search")
    print "searching ", song
    try:
        return soup.gethtml(song)
    except Exception as e:
        return "error getting " + song + ". error is:" + e.message

@application.route('/', methods = ['GET'])
def index():
    return render_template('index.html')

@application.route('/app.js', methods = ['GET'])
def appjs():
    return render_template('app.js')

if __name__ == '__main__':
    application.run(host=ipaddr, port=port, debug=False)
