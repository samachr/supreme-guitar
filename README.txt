How to run
==========
pip install -r ./requirements.txt
python server.py

visit http://localhost:8080 in a web browser

There is also a live version hosted on heroku at
http://supreme-guitar.herokuapp.com

Code Structure
==============

./templates/ 		The client web application files
./server.py		Flask based web server
./soup.py		interface to ultimate guitar using BeautifulSoup and urllib

./requirements.txt	The rest of these files are for install and heroku hosting
./runtime.txt
./app.json
./Procfile
./README.md
