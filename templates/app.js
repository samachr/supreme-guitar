function autoscroll(){
  var scrollPosition = window.scrollY;
  var intervalId = window.setInterval(function(){
    window.scrollBy(0, 1);
    if (window.scrollY == scrollPosition) {
      clearInterval(intervalId);
    } else {
      scrollPosition = window.scrollY
    }
  },30)
}

var intervalId;
function autoscrollwithsound(){
  var scrollPosition = window.scrollY;
  var scrollPosition = window.scrollX;
  var totalheight = $(document).height() - window.innerHeight;
  intervalId = window.setInterval(function(){
    if (window.scrollY == totalheight) {
      clearInterval(intervalId);
    } else {
    if (duration == 0) duration = player.getDuration();
    window.scrollTo(window.scrollX, totalheight * (player.getCurrentTime() / duration) * 1.2);
    }
  },30)
}

function search(song) {
  $("#topsongs").hide();
  var searchsong = song || $('#search_text').val()
  $('#search_text').val(unescape(searchsong));
  location.hash = escape(unescape(searchsong));
  $("#guitartabcontent").html("loading...");
  $("#submit_button").html('<span class="glyphicon glyphicon-time"></span>');

  player.stopVideo();
  playing = false;
  duration = 0;
  $("#youtubeControls").html('<span class="glyphicon glyphicon-play"></span>Youtube');
  clearInterval(intervalId);

  $.get("/api/find?search=" + encodeURIComponent(searchsong), function (data, status) {
    if (status == "success" && data.indexOf('http') == 0) {
      $("#guitartabcontent").html("found song at " + data + ". Loading it now...");
      $("#guitartabcontent").load("/api/song?search=" + encodeURIComponent(data), function(){
        $("#submit_button").html('<span class="glyphicon glyphicon-search"></span>');
        
        let a = document.querySelectorAll('a')
        player.loadPlaylist({ list: $('#search_text').val() + ' ' + a[a.length - 1].text + " lyrics", listType:"search"});
      });
    } else {
      $("#guitartabcontent").html("couldn't find the song you requested, the error message is: " + data);
    }
  });
}

var playing = false;
var duration = 0;
function togglevideo() {
  if(playing) {
    player.pauseVideo();
    playing = false;
    $("#youtubeControls").html('<span class="glyphicon glyphicon-play"></span>Youtube');
    clearInterval(intervalId);
  } else {
    player.playVideo();
    playing = true;
    $("#youtubeControls").html('<span class="glyphicon glyphicon-pause"></span>Youtube');
    autoscrollwithsound();
  }
}

$(function(){
  $('#search_text').keydown(function (event) {
      var keypressed = event.keyCode || event.which;
      if (keypressed == 13) {
          search()
      }
  });
});

var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubeIframeAPIReady() {
  player = new YT.Player('player', {
    height: '300',
    width: '300',
    videoId: '',
    origin: 'http://',
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {

}

// 5. The API calls this function when the player's state changes.
function onPlayerStateChange(event) {
  if (event.data == YT.PlayerState.PLAYING && !playing) {
    player.pauseVideo();
  } else if (event.data == YT.PlayerState.ENDED) {
    player.stopVideo();
  }
}

if (location.hash != "") {
  setTimeout(
    function(){
      search(location.hash.substring(1))
    }, 1000
  )
}