import sys
import urllib2
from bs4 import BeautifulSoup

def find_song(songname):
    print "finding ", songname
    url = "https://www.ultimate-guitar.com/search.php?search_type=title&order=&value=" + "+".join(songname.split(" "))
    req = urllib2.Request(url, headers={'User-Agent' : "Insert Browser Here"})
    con = urllib2.urlopen( req )
    soup = BeautifulSoup(con.read(), 'lxml')
    searchResults = soup.find("table", class_="tresults")

    highest = 0
    songlink = ""
    for tr in searchResults.findAll("tr"):
        if len(tr.findAll("td")) == 4:
            artist, song, rating, songtype = tr.findAll("td")
            songtype = songtype.find("strong").text
            if rating.text and (songtype == "chords" or songtype == "ukulele"):
                song_rating = int(rating.find("b", class_="ratdig").text)
                if song_rating > highest:
                    highest = song_rating
                    linkelement = song.find("a", class_="result-link")
                    if linkelement:
                        songlink = linkelement['href']
    return songlink

def gethtml(songlink):
    print "getting html for ", songlink
    url = songlink
    req = urllib2.Request(url, headers={'User-Agent' : "Insert Browser Here"})
    req.add_header('Referer', songlink)
    con = urllib2.urlopen( req )
    soup = BeautifulSoup(con.read(), 'html.parser')

    urlprefix = "https://tabs.ultimate-guitar.com"
    url = urlprefix + soup.find("a", class_="pr_b")['href']
    req = urllib2.Request(url, headers={'User-Agent' : "Insert Browser Here"})
    req.add_header('Referer', songlink)
    con = urllib2.urlopen( req )

    soup = BeautifulSoup(con.read(), 'html.parser')

    tab = [str(x) for x in soup.findAll("pre")]
    return ''.join(tab)

def grabtab(songname):
    print "grabbing tab for ", songname
    return gethtml(find_song("+".join(songname.split(" "))))